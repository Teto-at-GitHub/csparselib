﻿namespace FormatConverter
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.InputFormatBox = new System.Windows.Forms.ComboBox();
            this.InputFormatText = new System.Windows.Forms.Label();
            this.ConvertButton = new System.Windows.Forms.Button();
            this.OutputFormatText = new System.Windows.Forms.Label();
            this.OutputFormatBox = new System.Windows.Forms.ComboBox();
            this.InputFileLabel = new System.Windows.Forms.Label();
            this.btnInputFile = new System.Windows.Forms.Button();
            this.txtInputFIlePath = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // InputFormatBox
            // 
            this.InputFormatBox.AccessibleName = "";
            this.InputFormatBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.InputFormatBox.FormattingEnabled = true;
            this.InputFormatBox.Items.AddRange(new object[] {
            "BibTeX"});
            this.InputFormatBox.Location = new System.Drawing.Point(143, 59);
            this.InputFormatBox.Name = "InputFormatBox";
            this.InputFormatBox.Size = new System.Drawing.Size(121, 24);
            this.InputFormatBox.TabIndex = 0;
            this.InputFormatBox.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // InputFormatText
            // 
            this.InputFormatText.AutoSize = true;
            this.InputFormatText.Location = new System.Drawing.Point(30, 66);
            this.InputFormatText.Name = "InputFormatText";
            this.InputFormatText.Size = new System.Drawing.Size(91, 17);
            this.InputFormatText.TabIndex = 1;
            this.InputFormatText.Text = "Input format :";
            this.InputFormatText.Click += new System.EventHandler(this.label1_Click);
            // 
            // ConvertButton
            // 
            this.ConvertButton.Location = new System.Drawing.Point(395, 147);
            this.ConvertButton.Name = "ConvertButton";
            this.ConvertButton.Size = new System.Drawing.Size(97, 43);
            this.ConvertButton.TabIndex = 2;
            this.ConvertButton.Text = "Convert !";
            this.ConvertButton.UseVisualStyleBackColor = true;
            // 
            // OutputFormatText
            // 
            this.OutputFormatText.AutoSize = true;
            this.OutputFormatText.Location = new System.Drawing.Point(30, 107);
            this.OutputFormatText.Name = "OutputFormatText";
            this.OutputFormatText.Size = new System.Drawing.Size(107, 17);
            this.OutputFormatText.TabIndex = 3;
            this.OutputFormatText.Text = "Output Format :";
            // 
            // OutputFormatBox
            // 
            this.OutputFormatBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.OutputFormatBox.FormattingEnabled = true;
            this.OutputFormatBox.Location = new System.Drawing.Point(143, 100);
            this.OutputFormatBox.Name = "OutputFormatBox";
            this.OutputFormatBox.Size = new System.Drawing.Size(121, 24);
            this.OutputFormatBox.TabIndex = 4;
            // 
            // InputFileLabel
            // 
            this.InputFileLabel.AutoSize = true;
            this.InputFileLabel.Location = new System.Drawing.Point(30, 27);
            this.InputFileLabel.Name = "InputFileLabel";
            this.InputFileLabel.Size = new System.Drawing.Size(73, 17);
            this.InputFileLabel.TabIndex = 5;
            this.InputFileLabel.Text = "Input File :";
            this.InputFileLabel.Click += new System.EventHandler(this.label1_Click_1);
            // 
            // btnInputFile
            // 
            this.btnInputFile.Location = new System.Drawing.Point(498, 21);
            this.btnInputFile.Name = "btnInputFile";
            this.btnInputFile.Size = new System.Drawing.Size(75, 23);
            this.btnInputFile.TabIndex = 7;
            this.btnInputFile.Text = "...";
            this.btnInputFile.UseVisualStyleBackColor = true;
            this.btnInputFile.Click += new System.EventHandler(this.btnInputFile_Click);
            // 
            // txtInputFIlePath
            // 
            this.txtInputFIlePath.Location = new System.Drawing.Point(143, 22);
            this.txtInputFIlePath.Name = "txtInputFIlePath";
            this.txtInputFIlePath.Size = new System.Drawing.Size(349, 22);
            this.txtInputFIlePath.TabIndex = 8;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(585, 214);
            this.Controls.Add(this.txtInputFIlePath);
            this.Controls.Add(this.btnInputFile);
            this.Controls.Add(this.InputFileLabel);
            this.Controls.Add(this.OutputFormatBox);
            this.Controls.Add(this.OutputFormatText);
            this.Controls.Add(this.ConvertButton);
            this.Controls.Add(this.InputFormatText);
            this.Controls.Add(this.InputFormatBox);
            this.Name = "MainForm";
            this.Text = "Format Converter";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox InputFormatBox;
        private System.Windows.Forms.Label InputFormatText;
        private System.Windows.Forms.Button ConvertButton;
        private System.Windows.Forms.Label OutputFormatText;
        private System.Windows.Forms.ComboBox OutputFormatBox;
        private System.Windows.Forms.Label InputFileLabel;
        private System.Windows.Forms.Button btnInputFile;
        private System.Windows.Forms.TextBox txtInputFIlePath;
    }
}

