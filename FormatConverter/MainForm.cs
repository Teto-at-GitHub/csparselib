﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormatConverter
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void btnInputFile_Click(object sender, EventArgs e)
        {
            using (FolderBrowserDialog folder_browser = new FolderBrowserDialog()
                {
                    Description = "Select input file"
                })
                    {
                        if (folder_browser.ShowDialog() == DialogResult.OK)
                        {
                           txtInputFIlePath.Text = folder_browser.SelectedPath;  
                        }
                     }
        }
    }
}
